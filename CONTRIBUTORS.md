Contributors
============

* Mike Miller
* Colin Macdonald
* Juan Pablo Carbajal
* Abhinav Tripathi
* NVS Abhilash
* Vijay Krishnavanshi

---

Thanks to the authors of the original Pytave project, which served as
the origin and inspiration of this project:

* David Grundberg
* Håkan Fors Nilsson
* Jaroslav Hájek
